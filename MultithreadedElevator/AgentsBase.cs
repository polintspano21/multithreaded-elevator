﻿namespace MultithreadedElevator
{
    internal class AgentsBase
    {
        public readonly List<SecretAgent> AgentsInBase;
        private readonly BaseElevator _baseElevator;
        private readonly Dictionary<int, Floor> _floors;
        private readonly Random _random;

        public AgentsBase()
        {
            _random = new Random();
            _floors = new()
            {
                {0, new Floor(0, Security.CONFIDENTIAL, "G") },
                {1, new Floor(1, Security.SECRET, "S") },
                {2, new Floor(2, Security.TOP_SECRET, "T1") },
                {3, new Floor(3, Security.TOP_SECRET, "T2") }
            };
            _baseElevator = new BaseElevator(_floors, this);
            AgentsInBase = CreateAgents();
            foreach(KeyValuePair<int, Floor> floor in _floors)
            {
                floor.Value.LinkElevator(_baseElevator);
            }

        }

        private List<SecretAgent> CreateAgents()
        {
            List<SecretAgent> generatedAgents = new();

            for (int i = 0; i < 4; i++)
            {
                generatedAgents.Add(new SecretAgent($"Agent {i}", _baseElevator, _floors[0], _floors[_random.Next(0, 3)], (Security)_random.Next(0, 3), _floors));
            }

            return generatedAgents;
        }

        public void StartBase()
        {
            List<Thread> agentsThreads = new();

            AgentsInBase.ForEach((agent) =>
            {
                Thread agentThread = new(agent.StartWorkDay);
                agentsThreads.Add(agentThread);
                agentThread.Start();
            });

            Thread elevatorThread = new(_baseElevator.StartElevator);
            elevatorThread.Start();
            AgentsInBase.ForEach((agent) =>
            {
                _floors[0].CallElevator();
            });
            agentsThreads.ForEach((agentThread) => agentThread.Join());
            elevatorThread.Join();
            Console.WriteLine("Work is over!");
        }
    }
}
