﻿namespace MultithreadedElevator
{
    internal class BaseElevator
    {
        private ElevatorDirection _elevatorDirection;
        private readonly Semaphore _elevator;
        private readonly Queue<int> _destinationQueue;
        private readonly Queue<int> _callQueue;
        private readonly Dictionary<int, Floor> _floors;
        private Floor _currentFloor;
        private SecretAgent? _agentInElevator;
        private readonly AgentsBase _agentsBase;
        public bool AllWorkIsDone { get; private set; }

        public BaseElevator(Dictionary<int, Floor> floors, AgentsBase agentsBase)
        {
            _floors = floors;
            _destinationQueue = new Queue<int>();
            _callQueue = new Queue<int>();
            _elevator = new Semaphore(1, 1);
            _currentFloor = _floors[0];
            _agentsBase = agentsBase;
            AllWorkIsDone = false;
            _elevatorDirection = ElevatorDirection.STOPPED;
        }

        public void CallElevatorButton(int callingFloorNumber)
        {
            _callQueue.Enqueue(callingFloorNumber);
        }

        public bool TryEnterSecretAgent(SecretAgent secretAgent)
        {
            if (_elevator.WaitOne(0))
            {
                _agentInElevator = secretAgent;
                secretAgent.IsInElevator = true;
                Console.WriteLine($"{secretAgent.Name} entered the elevator headed for {secretAgent.DestinationFloor.Name}.");
                Console.WriteLine($"{secretAgent.DestinationFloor.Name} was added to the Queue.");
                _destinationQueue.Enqueue(secretAgent.DestinationFloor.FloorNumber);     

                return true;
            }
            else
            {
                return false;
            }
        }

        public void MoveElevator()
        {
            if (_destinationQueue.TryPeek(out int destination))
            {
                int direction = _currentFloor.FloorNumber - destination;

                if (direction < 0)
                {
                    _currentFloor = _floors[_currentFloor.FloorNumber + 1];
                    _elevatorDirection = ElevatorDirection.UP;
                    Console.WriteLine($"Elevator moves {_elevatorDirection}");
                }
                else if (direction > 0)
                {
                    _elevatorDirection = ElevatorDirection.DOWN;
                    _currentFloor = _floors[_currentFloor.FloorNumber - 1];
                    Console.WriteLine($"Elevator moves {_elevatorDirection}");
                }
                else
                {
                    _elevatorDirection = ElevatorDirection.STOPPED;
                    Console.WriteLine($"Elevator has {_elevatorDirection} on floor {_currentFloor.Name}");

                    if (_agentInElevator?.SecurityLevel >= _currentFloor.SecurityLevel && _agentInElevator.DestinationFloor.FloorNumber == _currentFloor.FloorNumber)
                    {
                        Console.WriteLine($"{_agentInElevator.Name} has left the elevator");
                        _agentInElevator.ArrivedAtDestination();
                        _destinationQueue.Dequeue();
                        _elevator.Release();
                    }

                    if (_agentInElevator?.SecurityLevel < _currentFloor.SecurityLevel && _agentInElevator.DestinationFloor.FloorNumber == _currentFloor.FloorNumber)
                    {
                        _agentInElevator.ChangeDestionation(_currentFloor.FloorNumber);
                        Console.WriteLine($"{_agentInElevator.Name} changed his destination to floor {_agentInElevator.DestinationFloor.Name}");
                        _destinationQueue.Dequeue();
                        _destinationQueue.Enqueue(_agentInElevator.DestinationFloor.FloorNumber);
                    }

                    _agentsBase.AgentsInBase.ForEach((agent) =>
                    {
                        if (agent.CurrentFloor.FloorNumber == _currentFloor.FloorNumber && agent.ShouldCallElevator)
                        {
                            if (agent.CanEnterElevator())
                            {
                                _callQueue.Dequeue();
                            }
                        };
                    });
                }
            }
            else if (_callQueue.TryPeek(out int callDestination))
            {
                int direction = _currentFloor.FloorNumber - callDestination;

                if (direction < 0)
                {
                    _currentFloor = _floors[_currentFloor.FloorNumber + 1];
                    _elevatorDirection = ElevatorDirection.UP;
                    Console.WriteLine($"Elevator moves {_elevatorDirection}");
                }
                else if (direction > 0)
                {
                    _elevatorDirection = ElevatorDirection.DOWN;
                    _currentFloor = _floors[_currentFloor.FloorNumber - 1];
                    Console.WriteLine($"Elevator moves {_elevatorDirection}");
                }
                else
                {
                    _elevatorDirection = ElevatorDirection.STOPPED;
                    Console.WriteLine($"Elevator has {_elevatorDirection} on floor {_currentFloor.Name}");

                    _agentsBase.AgentsInBase.ForEach((agent) =>
                    {
                        if (agent.CurrentFloor.FloorNumber == _currentFloor.FloorNumber && agent.ShouldCallElevator)
                        {
                            if (agent.CanEnterElevator())
                            {
                                _callQueue.Dequeue();
                            }
                        };
                    });
                }
            }

            if (_agentsBase.AgentsInBase.All((agent) => !agent.HasWorkToDo))
            {
                AllWorkIsDone = true;
            }
        }

        public void StartElevator()
        {
            while (!AllWorkIsDone)
            {
                Thread.Sleep(1000);
                MoveElevator();
            }
        }
    }
}
