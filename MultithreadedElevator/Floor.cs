﻿namespace MultithreadedElevator
{
    internal class Floor
    {
        public int FloorNumber { get; private set; }
        public string Name { get; private set; }
        public bool ElevatorIsCalled { get; private set; }
        public Security SecurityLevel { get; private set; }
        private BaseElevator? _baseElevator;
        public bool FloorIsQueued { get; set; }

        public Floor(int floorNumber, Security securityLevel, string name)
        {
            FloorNumber = floorNumber;
            ElevatorIsCalled = false;
            FloorIsQueued = false;
            SecurityLevel = securityLevel;
            Name = name;
        }

        public void LinkElevator(BaseElevator baseElevator)
        {
            _baseElevator = baseElevator;
        }

        public bool ShouldEnqueueFloor()
        {
            if (FloorIsQueued)
            {
                return false;
            }
            else
            {
                FloorIsQueued = true;
                return true;
            }
        }

        public void CallElevator()
        {
            _baseElevator?.CallElevatorButton(FloorNumber);
        }
    }
}
