﻿namespace MultithreadedElevator
{
    internal enum Security
    {
        CONFIDENTIAL,
        SECRET,
        TOP_SECRET
    }
}
