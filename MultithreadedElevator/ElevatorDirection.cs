﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultithreadedElevator
{
    internal enum ElevatorDirection
    {
        UP,
        STOPPED,
        DOWN
    }
}
