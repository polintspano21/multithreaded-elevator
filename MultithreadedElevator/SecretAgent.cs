﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultithreadedElevator
{
    internal class SecretAgent
    {
        public string Name { get; set; }
        public Floor CurrentFloor { get; set; }
        public Floor DestinationFloor { get; set; }
        public Security SecurityLevel { get; set; }
        public bool IsInElevator { get; set; }
        public bool ShouldCallElevator { get; set; }
        private Dictionary<int, Floor> _floors;
        public bool HasWorkToDo { get; set; }
        private readonly BaseElevator _baseElevator;

        public SecretAgent(string name, BaseElevator baseElevator, Floor currentFloor, Floor destinationFloor, Security securityLevel, Dictionary<int, Floor> floors)
        {
            Name = name;
            CurrentFloor = currentFloor;
            IsInElevator = false;
            ShouldCallElevator = true;
            _baseElevator = baseElevator;
            SecurityLevel = securityLevel;
            HasWorkToDo = true;
            DestinationFloor = destinationFloor;
            _floors = floors;
        }

        public bool CanEnterElevator()
        {
            return _baseElevator.TryEnterSecretAgent(this);
        }

        public Floor ChangeDestionation(int currentDestination)
        {
            int newDestination = 0;
            Random random = new();

            List<int> possibleDestinations = new()
            {
                0, 1, 2, 3
            };

            newDestination = possibleDestinations.FindAll((destination) => destination != currentDestination)[random.Next(0, 2)];
            DestinationFloor = _floors[newDestination];

            return DestinationFloor;
        }

        public void ArrivedAtDestination()
        {
            IsInElevator = false;
            ShouldCallElevator = false;

            Random random = new();
            if(random.Next(0, 100) > 50)
            {
                HasWorkToDo = false;
                Console.WriteLine($"{Name} is done working.");
            }

            SimulateWork();
        }

        private async void SimulateWork()
        {
            Random random = new();

            if (HasWorkToDo)
            {
                await Task.Delay(random.Next(2000, 8000));
                ChangeDestionation(CurrentFloor.FloorNumber);
                ShouldCallElevator = true;
                Console.WriteLine($"{Name} called elevator from floor {CurrentFloor.Name}");
                CurrentFloor.CallElevator();
            }
        }

        public void StartWorkDay()
        {
            while (HasWorkToDo) 
            {
                
            }
        }
    }
}
